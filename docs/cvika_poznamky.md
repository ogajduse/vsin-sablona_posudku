# Zakony

## CZ

* 36/1967 sb. - Zakon o znalcich a tlumocnicich
* 37/1967 sb. - Vyhlaska k provedeni zakona o znalcich a tlumocnicich
    - maju upravu 2006 a 2011

* 526/1990 sb. - Zakon o cenach (ZOC)
    - cena sjednana
    - cena urcena (definovana v ZOM ako obvykla, mimoradna, zjistena)
        - k regulacii cien
* 151/1997 sb. - Zakon o ocenovani majetku (ZOM)
    - cena obvykla
    - cena mimoradna
    - zjistena
        - specialny pravny predpis na ktory sa odkazuje v inych predpisoch k urceniu ceny


## Medzinarodne

* Internationas valuation standard (IVS)
* Evropska komise (EVS)
    - rozlisuju cenu a hodnotu

# Pojmy / vzorce

**CC** = cena casova (Hodnota nakladova/casova)

**ZA** = zakladna amortizacia vid amortizacne tabulky
- ZA = (1 - zbytkova hodnota z tabulky)

**S** = srazka, o kolko % sa to zhorsilo (porucha, nie uplna funkcnost)

**P** = prirazka, o kolko % som to vylepsil

**TH** = technicka hodnota
- TH = VHT * (1 - ZA) * (1 - S) * (1 + P)

**VHT** = vychozi technicka hodnota
- 100%, generalna oprava 90% a menej, po GO sa rata zivotnost zase od znovu

**KP** = koeficient prodejnosti

**CN** = cena nova (noveho)

**KTU** = koeficient technickej urovne
- aku hodnotu ma ocenovani voci substitutu (o kolko je horsi, generace, horsi HW)

**COB** = cena obvykla ocenovaneho

**CPs** = cena prodejni srovnatelneho
- cena noveho srovnatelneho (cena substitutu), tovarne bezvadne noveho

**CCs** = cena casova srovnatelneho
- casova cena substitutu (vypocet z noveho), vypocitava sa na zaklade odlisnosti od ocenovaneho pomocou **KO** (koeficient odlisnosti)
- KO sa urcuje na zaklade provedeni, stavu atd. oproti ocenovanemu, > 1 substitut je lepsi, < 1 substitut je horsi
```
CCs = CPs x KO
```

**FV** = future value

**PV** = present value

## Pristupy

### Nakladovy

```
CC = CN x TH
```

**CN**

* stale dostupny tovar v predaji = nakupna cena
* nedostupny na trhu, najdeme si najblizsi podobny v predaji (substitut) a znizime jeho cenu o generacie (lepsie parametre) 

```
CN = cena substitutu x KTU
TH = VHT * (1 - ZA) * (1 - S) * (1 + P)
ZA = (1 - zbytkova hodnota z tabulky)
```

### Vynosovy

**FV** = cisty vynos (odratane naklady)
- robi sa sumou jednotlivych rokov, pri predaji sa sa pripocita v danom roku k FV
```
PV = FV / q^pocet_rokov; q > 1
q = urocitel (kapitalizacny faktor)
q = 1 + i;
i = urokova miera (miera kapitalizacie); i < 1
```

**vecna renta**
```
PV = FV / i; i < 1
```

### Porovnavaci
```
COB = CC x KP
```

**KP** - disponujeme substitutmi,
```
KP = suma((CPs)i) / suma((CCs)i)
```

## Pomerna technicka hodnota skupiny (PTHS)
- rozdelime si stroj na podskupiny dielov (samostatne ocenitelne), a urcime ich PDS
- celkova technicka hodnota je suma PTHS skupin

**PDS** = pomerny diel skupiny % - ake ma zastupenie v celku

`suma((PDS)i) = 1 (100%)`

**THSN** = technicka hodnota skupiny nova (to iste ako VTH, akurat pre jednotlivu skupinu)

**PTHS** = pomerna technihodnota skupiny

**ZAs** = zakladni amortizace skupiny

**Ss** = srazka skupiny

**Ps** = prirazka skupiny

    
```
PTHS = THSN x (1 - ZAs) x (1 - Ss) x (1 + Ps) x PDS

TH = suma((PTHS)i); vsetky skupiny.
```
## Majetkova ujma

**VMU** = vyska majetkovej ujmy \
**C1** = cena obvykla (COB), cize urcujeme (CN -> TH -> CC -> KP -> COB) \
**NO** = naklady opravy \
**TH1** = technicka hodnota pred ujmou \
**TH2** = tehcnicka hodnota po oprave \
**CCMV** = cena casova mimoriadnej vybavy \

```
C1 < NO: 

    VMU = C1 - CZ
    
C1 > NO:

    TH1 = TH2 (oprava nema vplyv na cenu veci)
    
        VMU = NO - CZ
        - ak je mimoriadna vybava:
        VMU = NO + (CCMV1 - CCMV2) - CZ
        
    TH1 != TH2 (oprava ma vplyv na cenu veci)
    
        VMU = NO + (CC1 - CC2) - CZ
            CC1 = CN * TH1
            CC2 = CN * TH2
```
