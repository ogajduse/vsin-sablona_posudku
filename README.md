## skripta: 
[*OCEŇOVÁNÍ MOVITÉHO MAJETKU*](https://www.vutbr.cz/usi/studium/czv/studijni-opora-pro-ocenovani-moviteho-majetku-p89852?fbclid=IwAR35H-d21wcYGa3DpCHbH3HO-ORUit0hunrMhRMn33ehBqfu5e4j-Xn2t2E)
* 1. Základní východiska pro oceňování majetku
* 2. Pojmy cena a hodnota při oceňování majetku
* 3. Předmět oceňování
* 4. Systémové vymezení trhů 
* 5. Tvorba ceny na trhu
* 6. Obecná východiska pro stanovení užitné a směnné hodnoty výrobků
* 7. Základní pojmy pro oceňování motorových vozidel
* 8. Oceňování motorových vozidel
* 9. Zásady pro používání amortizačních stupnic a specifické případy při oceňování
* 10. Základní východiska pro stanovení výše majetkové újmy
* 11. Stanovení výše majetkové újmy vzniklé poškozením vozidla
* 12. Oceňování strojů a zařízení
* 13. Oceňování souborů hmotného movitého majetku
    
[*OBECNÁ METODIKA SOUDNÍHO INŽENÝRSTVÍ*](https://www.vutbr.cz/usi/studium/czv/studijni-opora-pro-obecnou-metodiku-soudniho-inzenyrstvi-p89829?fbclid=IwAR3jCsZ8xpuo0Y7-erVOCI3uKrHqf4wpkw7Ae02nkEAfoqvu86ZqyH085I8)
* 1. Soudní inženýrství a soudní znalectví
* 2. Právní úprava znalectví, pojem znalec
* 3. Řízení a organizace znalectví
* 4. Znalecký posudek
* 5. Znalecká činnost – legislativní požadavky
* 6. Praktický výkon znalecké činnosti
* 7. Podíl znalce na zajištění důkazů
* 8. Experiment a ohledání při činnosti znalce
* 9. Aplikace předpisů ve znaleckém posudku
* 10. Odpovědnost znalce za podaný posudek a další sankce
* 11. Přibrání znalce k podání posudku
* 12. Odměňování a náhrada nákladů
* 13. Úvod do speciálních metodik soudního inženýrství

# VSIN-sablona_posudku

Predpripravená šablóna na písanie cvičného znaleckého posudku pre potreby predmetu VSIN vyučovanom na VUT FEKT. Obsahuje predprípravu pre slovenskú a českú verziu.

Posudok by mal obsahovat:

* Uvod
	- Nieje povinna, ale je vhodne ju mat
	- Nieje cislovana
	- Na zaklade akeho poziadavku je posudok spracovany
	- Opis znaleckeho ukolu	

* Nalez
	- Povinna cast
	- VZT: v posudku uvedie znalec popis skumaneho materialu, popripadne javu, suhrn skutocnosti, k nimz pri ukone prihliadal
		informacie a skutocnosti z ktorych vychadzal
		u prepokladu oznacenie, datum, cisla listov spisovych podkladov
		vytah podstatnych udajov	
	- Logicke clenenie
	- Musi byt uvedene aj to, co pripadne nebude suhlasit so zaverom znaleckeho posudku
	- znalec nesmie vybrat iba pre jeho riesenie vhodne podklady	

* Posudok
	- VZT: vycet otazok na ktore ma odpovedat s odpovedami na tieto otazky
	- Vymedzenie pojmov
	- Technicka prijatelnost podkladov
	- Popis metody riesenia
		u standardizovanych metod staci odkaz
	- Vstupne udaje pre riesenie
	- Vlastne riesenie
	- Vysledky a zaver
	- Povinne uvedene otazky a odpovede na ne	
	
* Prilohy
	- Zoznam sa uvadza za kapitolu posudok
	- Protokoly z vypoctovych SW
	- Stanovenie koeficientu predajnosti
	- Nekopirovat listy spisu ...
	- Oznacenie priloh Priloha c.: , list xx
	- Zvazuju sa s posudkom za otiskom pecate a znaleckou dolozkou

others:
	Prehliadka + jej fotodokumentacia a opis kde,kedy atd.
	Funkcna skuska + opis zisten (nedestruktivna)

